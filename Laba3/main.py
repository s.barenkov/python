import sys

import cv2 as cv
import numpy as np

DIR = r'E:\img.jpg'
DIR_save = r'E:'

img = cv.imread(DIR)  # чтение
if type(img) is not np.ndarray:
    sys.exit("Error: the image has not been correctly loaded.")
cv.imshow('Image', img)  # вывод картинки

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)  # реформат в серый
cv.imshow('Gray', gray)  # вывод серой картинки


def translate(img, x, y):
    """
    translate the image
    :param img: image
    :param x: translate to x scale
    :param y: translate to y scale
    :return:
    """
    trans_mat = np.float32([[1, 0, x], [0, 1, y]])
    print('Translate matrix: {}'.format(trans_mat))
    dimensions = (img.shape[1], img.shape[0])
    print('Translate dimensions: {}'.format(dimensions))
    return cv.warpAffine(img, trans_mat, dimensions)


def shift(img, x, y):
    """
    shift the image
    :param img: image
    :param x: shift to x scale
    :param y: shift to y scale
    :return:
    """
    trans_mat = np.float32([[1, x, 0], [y, 1, 0]])
    print('Shift matrix: {}'.format(trans_mat))
    dimensions = (img.shape[1], img.shape[0])
    print('Shift dimensions: {}'.format(dimensions))
    return cv.warpAffine(img, trans_mat, dimensions)


def rotate(img, angle, rot_point=None):
    """
    rotate the image
    :param img: image
    :param angle: angle in which we want to rotate our img
    :param rot_point: center of rotation 2D matrix
    :return:
    """
    (height, width) = img.shape[:2]
    if rot_point is None:
        rot_point = (width // 2, height // 2)
        print('rotate center: {}'.format(rot_point))
    rot_mat = cv.getRotationMatrix2D(rot_point, angle, 1.0)
    print('rotate matrix: {}'.format(rot_mat))
    dimensions = (width, height)
    print('rotate dimension: {}'.format(dimensions))
    return cv.warpAffine(img, rot_mat, dimensions)


translated = translate(img, 100, 100)  # вызов функции смещения, где смещение идет на 100 px по x и y
cv.imshow('Translated', translated)  # вывод перемещенной картинки

rotated = rotate(img, 45)  # вызов функции наклона, где угол 45 градусов
cv.imshow('Rotated', rotated)  # вывод повернутой картинки

resized = cv.resize(img, (500, 500), interpolation=cv.INTER_CUBIC)  # меняем размер картинки, где (500, 500) размер
(height, width) = resized.shape[:2]
print('resized img size {}, {}'.format(width, height))
cv.imshow('Resized', resized)  # вывод промаштабируемой картинки

# """переворачиваем картинку, где 2 значение может быть (0, 1, -1), 0 по вертикали, 1 по горизонтали,
#  -1 и горизонтали и вертикли"""
# flip = cv.flip(img, 0)  # переворот картинки
# cv.imshow('Flip', flip)  # вывод перевернутой картинки

shifted = shift(img, 1, 2)
cv.imshow('Shifted', shifted)


def rgb_split():
    (B, G, R) = cv.split(img)  # разделил каналы
    zeros = np.zeros(img.shape[:2], dtype="uint8")  # создаем массив нулей
    red = cv.merge([zeros, zeros, R])
    green = cv.merge([zeros, G, zeros])
    blue = cv.merge([B, zeros, zeros])
    cv.imshow("Red", red)  # красный канал
    cv.imshow("Green", green)  # зеленый канал
    cv.imshow("Blue", blue)  # синий канал

    # cv.imwrite(DIR_save, red)  # сохранение красного канала
    # cv.imwrite(DIR_save, green)  # сохранение зеленого канала
    # cv.imwrite(DIR_save, blue)  # сохранение синего канала


# rgb_split()


cv.waitKey(0)  # ожидание нажатие клавиши
cv.destroyAllWindows()  # закрываем все окна
