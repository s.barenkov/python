import cv2 as cv
import numpy as np


def reading_img(file_name):
    img = cv.imread(file_name)
    return img


def translation(img, x, y):
    """
    translate the image
    :param img: image
    :param x: translate to x scale
    :param y: translate to y scale
    :return: affine transformation: translate
    """
    trans_mat = np.float32([[1, 0, x], [0, 1, y]])
    print('Translate matrix: {}'.format(trans_mat))
    dimensions = (img.shape[1], img.shape[0])
    print('Translate dimensions: {}'.format(dimensions))
    return cv.warpAffine(img, trans_mat, dimensions)


def shifting(img, x, y):
    """
    shift the image
    :param img: image
    :param x: shift to x scale
    :param y: shift to y scale
    :return: affine transformation: shifting
    """
    trans_mat = np.float32([[1, x, 0], [y, 1, 0]])
    print('Shift matrix: {}'.format(trans_mat))
    dimensions = (img.shape[1]*3, img.shape[0]*4)
    print('Shift dimensions: {}'.format(dimensions))
    return cv.warpAffine(img, trans_mat, dimensions)


def rotating(img, angle, rot_point):
    """
    rotate the image
    :param img: image
    :param angle: angle in which we want to rotate our img
    :param rot_point: center of rotation 2D matrix
    :return: affine transformation: rotating
    """
    (height, width) = img.shape[:2]
    if rot_point == (0, 0):
        rot_point = (width // 2, height // 2)
        print('rotate center: {}'.format(rot_point))
    rot_mat = cv.getRotationMatrix2D(rot_point, angle, 1.0)
    print('rotate matrix: {}'.format(rot_mat))
    dimensions = (width, height)
    print('rotate dimension: {}'.format(dimensions))
    return cv.warpAffine(img, rot_mat, dimensions)


def rescaling(frame, scale):
    """
    rescale image
    :param frame: image
    :param scale: scale in which change your image
    :return: resized image
    """
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimensions = (width, height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


def rgb_split(color, img):
    """
    split rgb channel
    :param color: channel case
    :param img: image
    :return: img with color channel
    """
    (B, G, R) = cv.split(img)
    zeros = np.zeros(img.shape[:2], dtype="uint8")
    if color == 'red':
        red = cv.merge([zeros, zeros, R])
        return red
    elif color == 'green':
        green = cv.merge([zeros, G, zeros])
        return green
    elif color == 'blue':
        blue = cv.merge([B, zeros, zeros])
        return blue

