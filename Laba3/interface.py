import cv2
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import QFileDialog, QInputDialog, QMessageBox
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import actions


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1366, 720)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("CentralWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("HorizontalLayout")
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setObjectName("ScrollArea")
        self.label = QtWidgets.QLabel()
        self.label.setText("")
        self.label.setObjectName("Label")
        self.label.setAlignment(Qt.AlignTop)
        self.label.setGeometry(20, 20, 3840, 2160)
        self.scroll_area.setWidget(self.label)
        self.horizontalLayout.addWidget(self.scroll_area)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setObjectName("Menubar")
        self.menu_file = QtWidgets.QMenu(self.menubar)
        self.menu_file.setObjectName("MenuFile")
        self.menu_rgb_to_gray = QtWidgets.QAction(MainWindow)
        self.menu_rgb_to_gray.setObjectName("MenuGray")
        self.menu_affine_transformations = QtWidgets.QMenu(self.menubar)
        self.menu_affine_transformations.setObjectName("MenuAffineTransformations")
        self.menu_channels = QtWidgets.QMenu(self.menubar)
        self.menu_channels.setObjectName("MenuChannels")
        self.menu_clear = QtWidgets.QAction(self.menubar)
        self.menu_clear.setObjectName("MenuClear")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("Statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action_open = QtWidgets.QAction(self.centralwidget)
        self.action_open.setObjectName("Open")
        self.action_save = QtWidgets.QAction(MainWindow)
        self.action_save.setObjectName("Save")
        self.action_close = QtWidgets.QAction(MainWindow)
        self.action_close.setObjectName("Close")
        self.action_translating = QtWidgets.QAction(MainWindow)
        self.action_translating.setCheckable(False)
        self.action_translating.setObjectName("Translate")
        self.action_scaling = QtWidgets.QAction(MainWindow)
        self.action_scaling.setObjectName("Scaling")
        self.action_rotating = QtWidgets.QAction(MainWindow)
        self.action_rotating.setObjectName("Rotating")
        self.action_shifting = QtWidgets.QAction(MainWindow)
        self.action_shifting.setObjectName("Shift")
        self.action_red = QtWidgets.QAction(MainWindow)
        self.action_red.setObjectName("RED")
        self.action_green = QtWidgets.QAction(MainWindow)
        self.action_green.setObjectName("GREEN")
        self.action_blue = QtWidgets.QAction(MainWindow)
        self.action_blue.setObjectName("BLUE")
        self.menu_file.addAction(self.action_open)
        self.menu_file.addAction(self.action_save)
        self.menu_file.addAction(self.action_close)
        self.menu_affine_transformations.addAction(self.action_translating)
        self.menu_affine_transformations.addAction(self.action_scaling)
        self.menu_affine_transformations.addAction(self.action_rotating)
        self.menu_affine_transformations.addAction(self.action_shifting)
        self.menu_channels.addAction(self.action_red)
        self.menu_channels.addAction(self.action_green)
        self.menu_channels.addAction(self.action_blue)
        self.menubar.addAction(self.menu_file.menuAction())
        self.menubar.addAction(self.menu_rgb_to_gray)
        self.menubar.addAction(self.menu_affine_transformations.menuAction())
        self.menubar.addAction(self.menu_channels.menuAction())
        self.retranslateUi(MainWindow)
        self.action_open.triggered.connect(self.loading)
        self.action_save.triggered.connect(self.saving)
        self.action_close.triggered.connect(self.closing)
        self.menu_rgb_to_gray.triggered.connect(self.rgb_to_gray)
        self.action_translating.triggered.connect(self.translating)
        self.action_scaling.triggered.connect(self.scaling)
        self.action_rotating.triggered.connect(self.rotating)
        self.action_shifting.triggered.connect(self.shifting)
        self.action_red.triggered.connect(self.rgb_split_to_red)
        self.action_green.triggered.connect(self.rgb_split_to_green)
        self.action_blue.triggered.connect(self.rgb_split_to_blue)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.file_name = None
        self.tmp = None

    def loading(self):
        """
        Load img
        :return: exit from func if img is not chosen
        """
        self.file_name = QFileDialog.getOpenFileName(filter="Image (*.*)")[0]
        if len(self.file_name) > 0:
            self.img = cv2.imread(self.file_name)
            if type(self.img) is not np.ndarray:
                self.img_checking(self.img)
                return
            self.setting(self.img)

    def setting(self, img):
        """
        set image in lable for output on the screen
        :param img: our image
        """
        self.tmp = img
        frame = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = QImage(frame, frame.shape[1], frame.shape[0], frame.strides[0], QImage.Format_RGB888)
        self.label.setPixmap(QtGui.QPixmap.fromImage(img))

    def saving(self):
        """
        saving pic in folder which you want
        :return: exit from def if img is not loaded
        """
        if self.img_checking(self.tmp):
            return
        self.file_name = QFileDialog.getSaveFileName(filter="JPG(*.jpg);;PNG(*.png);;TIFF(*.tiff);;BMP(*.bmp)")[0]
        if len(self.file_name) > 0:
            cv2.imwrite(self.file_name, self.tmp)
            print('Image saved as:', self.file_name)

    def rgb_to_gray(self):
        """
        transform bgr to gray img
        :return: exit from def if img is not loaded
        """
        img = actions.reading_img(self.file_name)
        if self.img_checking(img):
            return
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        self.setting(gray)

    def translating(self):
        """
        translate the img
        :return: exit from def if img is not loaded
        """
        x, x_return = QInputDialog.getInt(self.centralwidget, 'Input X', 'Enter number of pix to translate img:')
        y, y_return = QInputDialog.getInt(self.centralwidget, 'Input Y', 'Enter number of pix to translate img:')
        if x_return and y_return:
            img = actions.reading_img(self.file_name)
            if self.img_checking(img):
                return
            frame = actions.translation(img, x, y)
            self.setting(frame)

    def scaling(self):
        """
        scale the image
        :return: exit from def if img is not loaded
        """
        scale, scale_return = QInputDialog.getDouble(self.centralwidget, 'Input scale',
                                                     'Enter scale which should be more then 0')
        if scale_return:
            if scale > 0:
                img = actions.reading_img(self.file_name)
                if self.img_checking(img):
                    return
                frame = actions.rescaling(img, scale)
                self.setting(frame)
            else:
                self.digit_checking()
                self.scaling()

    def shifting(self):
        """
        shifting the image
        :return: exit from def if img is not loaded
        """
        x, x_return = QInputDialog.getInt(self.centralwidget, 'Input X', 'Enter scale to shifting img:')
        y, y_return = QInputDialog.getInt(self.centralwidget, 'Input Y', 'Enter scale to shifting img:')
        if x_return and y_return:
            if x > 0 and y > 0:
                img = actions.reading_img(self.file_name)
                if self.img_checking(img):
                    return
                frame = actions.shifting(img, x, y)
                self.setting(frame)
            else:
                self.digit_checking()
                self.shifting()

    def rotating(self):
        """
        rotating the image
        :return: exit from def if img is not loaded
        """
        width, width_return = QInputDialog.getInt(self.centralwidget, 'Input width',
                                                  'Enter the X pix count to chose point for rotating, 0 should be '
                                                  'default:')
        height, height_return = QInputDialog.getInt(self.centralwidget, 'Input height',
                                                    'Enter the Y pix count to chose point for rotating, 0 should be '
                                                    'default:')
        angle, angle_return = QInputDialog.getInt(self.centralwidget, 'Input angle', 'Enter the angle for rotating:')
        if width_return and height_return and angle_return:
            img = actions.reading_img(self.file_name)
            if self.img_checking(img):
                return
            rot_point = (width, height)
            frame = actions.rotating(img, angle, rot_point)
            self.setting(frame)

    def rgb_split_to_red(self):
        """
        create red channel
        :return: exit from def if img is not loaded
        """
        img = actions.reading_img(self.file_name)
        if self.img_checking(img):
            return
        frame = actions.rgb_split('red', img)
        self.setting(frame)

    def rgb_split_to_green(self):
        """
        create green channel
        :return: exit from def if img is not loaded
        """
        img = actions.reading_img(self.file_name)
        if self.img_checking(img):
            return
        frame = actions.rgb_split('green', img)
        self.setting(frame)

    def rgb_split_to_blue(self):
        """
        create blue channel
        :return: exit from def if img is not loaded
        """
        img = actions.reading_img(self.file_name)
        if self.img_checking(img):
            return
        frame = actions.rgb_split('blue', img)
        self.setting(frame)

    def closing(self):
        """
        closing img in lable
        """
        self.label.clear()
        self.tmp = None
        self.file_name = None

    # def cancel(self):
    #     """
    #     check on action cancel
    #     """
    #     msg = QMessageBox()
    #     msg.setWindowTitle("Cancel dialog")
    #     msg.setText("action is canceled")
    #     # msg.setIcon(QMessageBox.Information)
    #     msg.exec_()

    def img_checking(self, image):
        """
        check img for open
        :param image: our image
        :return: True
        """
        if type(image) is not np.ndarray:
            msg = QMessageBox()
            msg.setWindowTitle("Error dialog")
            msg.setText("Image not selected")
            # msg.setIcon(QMessageBox.Warning)
            msg.exec_()
            return True

    def digit_checking(self):
        msg = QMessageBox()
        msg.setWindowTitle("Error dialog")
        msg.setText("Wrong digit, try again!")
        # msg.setIcon(QMessageBox.Warning)
        msg.exec_()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Laba3"))
        self.menu_file.setTitle(_translate("MainWindow", "File"))
        self.menu_rgb_to_gray.setText(_translate("MainWindow", "RGB to Gray"))
        self.menu_affine_transformations.setTitle(_translate("MainWindow", "Affine transformations"))
        self.menu_channels.setTitle(_translate("MainWindow", "Channels"))
        self.action_open.setText(_translate("MainWindow", "Open"))
        self.action_save.setText(_translate("MainWindow", "Save"))
        self.action_close.setText(_translate("MainWindow", "Close"))
        self.action_translating.setText(_translate("MainWindow", "Translating"))
        self.action_scaling.setText(_translate("MainWindow", "Scaling"))
        self.action_rotating.setText(_translate("MainWindow", "Rotating"))
        self.action_shifting.setText(_translate("MainWindow", "Shifting"))
        self.action_red.setText(_translate("MainWindow", "Red"))
        self.action_green.setText(_translate("MainWindow", "Green"))
        self.action_blue.setText(_translate("MainWindow", "Blue"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
