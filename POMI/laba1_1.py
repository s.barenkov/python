"""Task 1 Lab 1"""
import logging

logging.basicConfig(filename='logging.log',
                    format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)
logger = logging.getLogger()


def square(size, start_pos):
    """
    print square from █ symbol
    :param size: size of square
    :param start_pos: position from which start printing
    """
    for i in range(size):
        logger.info('print square')
        print(' '*start_pos*2 + "█" * size * 2)


while True:
    choice = input('Choice a figure: 1-square;\n 2-pyramid;\n 3-Exit;\n')
    if choice == '1':  # square
        while True:
            size = input('Enter your size: ')
            if size.isdigit():
                size = int(size)
                break
            else:
                logger.warning('not a digit')
                print('Something went wrong! ¯\_(ツ)_/¯ Try again!')
        square(size, 0)
    elif choice == '2':  # pyramid
        while True:
            size = input('Enter your size: ')
            if size.isdigit():
                size = int(size)
                break
            else:
                logger.warning('not a digit')
                print('Something went wrong! ¯\_(ツ)_/¯ Try again!')
        start_pos = 0
        logger.info('print pyramid')
        for i in range(size, 0, -2):
            square(i, start_pos)
            start_pos += 1
    elif choice == '3':  # exit
        logger.info('exit from program')
        print('Exit!')
        raise SystemExit(1)
    else:
        logger.warning('Something went wrong!')
        print('Something went wrong! ¯\_(ツ)_/¯ Try again!')
