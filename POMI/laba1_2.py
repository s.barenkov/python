import random
import logging

logging.basicConfig(filename='logging.log',
                    format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)
logger = logging.getLogger()


def print_arr(arr):
    """
    printing the game cell
    :param arr: list with number of cell
    """
    logger.info('print cell')
    print(f'''-------------------------
|       |       |       |
|   {arr[0]}   |   {arr[1]}   |    {arr[2]}  |
|       |       |       |
-------------------------
|       |       |       |
|   {arr[3]}   |   {arr[4]}   |    {arr[5]}  |
|       |       |       |
-------------------------
|       |       |       |
|   {arr[6]}   |   {arr[7]}   |    {arr[8]}  |
|       |       |       |
-------------------------''')


empty_cell = 8
arr = ['1', '2', '3', '4', 'X', '6', '7', '8', '9']
print_arr(arr)
while empty_cell > 0:
    while True:
        while True:
            print('select the cell')
            cell = input()
            if cell.isdigit():
                cell = int(cell)
                break
            else:
                logger.warning('not a digit')
                print('Something went wrong! ¯\_(ツ)_/¯ Try again!')
        if cell >= 10 or cell <= 0:
            logger.warning('out of range')
            print('Something went wrong! ¯\_(ツ)_/¯ Try again!')
        elif arr[cell-1] == "O" or arr[cell-1] == "X":
            logger.warning('the same cell')
            print('Something went wrong! ¯\_(ツ)_/¯ Try again!')
        else:
            arr[cell-1] = 'O'
            empty_cell -= 1
            break
    print_arr(arr)
    """check all ways to win for player"""
    player_win_horizontal = (arr[0] == arr[1] == arr[2]) or (arr[3] == arr[4] == arr[5]) or (arr[6] == arr[7] == arr[8])
    player_win_vertical = (arr[0] == arr[3] == arr[6]) or (arr[1] == arr[4] == arr[7]) or (arr[2] == arr[5] == arr[8])
    if player_win_horizontal or player_win_vertical:
        logger.info('player win')
        print('Player wins!')
        break
    while True:
        rand = int(random.random()*9+1)
        if (rand > 0 or rand < 10) and (arr[rand-1] != "O" and arr[rand-1] != "X"):
            print('Computer select the cell number: {}'.format(rand))
            arr[rand-1] = 'X'
            empty_cell -= 1
            break
    print_arr(arr)
    """check all ways to win for computer"""
    AI_win_vertical = (arr[0] == arr[3] == arr[6]) or (arr[1] == arr[4] == arr[7]) or (arr[2] == arr[5] == arr[8])
    AI_win_diagonal = (arr[0] == arr[4] == arr[8]) or (arr[2] == arr[4] == arr[6])
    AI_win_horizontal = (arr[0] == arr[1] == arr[2]) or (arr[3] == arr[4] == arr[5]) or (arr[6] == arr[7] == arr[8])
    if AI_win_horizontal or AI_win_vertical or AI_win_diagonal:
        logger.info('computer win')
        print('Computer wins!')
        break
else:
    logger.info('Draw')
    print('Draw')
