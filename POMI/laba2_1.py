"""Task 1 Lab 2"""
import logging
from os import path

logging.basicConfig(filename='logging.log',
                    format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)
logger = logging.getLogger()


class BadLineError(BaseException):
    pass


class EmptyFileError(BaseException):
    pass


def name_mark(line, info):
    """
    fill the list from file with name and marks, check on bed string error
    :param line: line in the file
    :param info: list for name and marks
    :return: list filled names and marks
    """
    i = -1
    mark = ''
    while line[i] != ' ':
        mark = line[i] + mark
        i -= 1
    mark = mark.replace('\n', '')
    if not mark.replace('.', '', 1).isdigit():
        logger.info('raise the bad string error')
        raise BadLineError
    full_name = line[0: len(line) + i]
    if not full_name.replace(' ', '', 1).isalpha():
        logger.info('raise the  bad string error')
        raise BadLineError
    information = [full_name, mark]
    info.append(information)
    return info


def sum_of_marks(info):
    """
    count sum of marks for the same name
    :param info: list with name and marks
    :return: list where the func count the sum
    """
    i = 0
    while i != len(info):
        j = i + 1
        while j != len(info):
            if info[i][0] == info[j][0]:
                info[i][1] = float(info[i][1]) + float(info[j][1])
                info.pop(j)
                j -= 1
            j += 1
        i += 1
    return info


file_name = input('Enter file name: ')
try:
    """open file and close as soon as we exit from loop"""
    with open('{}.txt'.format(file_name)) as f:
        if path.getsize('{}.txt'.format(file_name)) == 0:
            logger.info('Checking on empty file')
            raise EmptyFileError
        info = []
        print('content from file: ')
        for line in f:
            name_mark(line, info)
            print(line.rstrip('\n'))
    sum_of_marks(info)
    print('-----------------')
    print('summary of marks: ')
    i = 0
    while i < len(info):
        print(info[i][0] + " " + str(info[i][1]))
        i += 1
except FileNotFoundError:
    logger.error('ERROR! File not found!')
    print('File doesn\'t exist')
except BadLineError:
    logger.error('ERROR! Bad line!')
    print('Wrong line found')
except EmptyFileError:
    logger.info('ERROR! File is empty!')
    print('File is empty')
