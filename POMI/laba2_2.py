"""Task 2 Lab 2"""
from os import path
import logging

logging.basicConfig(filename='logging.log',
                    format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)
logger = logging.getLogger()


class EmptyFileError(BaseException):
    pass


def counter(line):
    """
    count the number of character which enter in file
    :param line: line in file
    :return: histogram of
    """
    histogram = []
    i = 0
    while i != len(line):
        j = 0
        found = False
        while j != len(histogram):
            if line[i] == histogram[j][0]:
                histogram[j][1] += 1
                found = True
                break
            j += 1
        if not found:
            histogram.append([line[i], 1])
        i += 1
    return sorted(histogram)


file_name = input('enter file name: ')
try:
    with open('{}.txt'.format(file_name)) as f:
        if path.getsize('{}.txt'.format(file_name)) == 0:
            logger.info('Checking on empty file')
            raise EmptyFileError
        for line in f:
            print(line.rstrip('\n'))
            f = filter(str.isalpha, line)
            line = "".join(f).lower()
            stat = counter(line)
            k = 0
            while k != len(stat):
                print(str(stat[k][0]) + " " + str(stat[k][1]))
                k += 1

except FileNotFoundError:
    logger.error('ERROR! file not found!')
    print("File doesn't exist")
except EmptyFileError:
    logger.error('ERROR! file is empty!')
    print("File is empty")
