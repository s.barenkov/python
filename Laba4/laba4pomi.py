import cv2
import imutils
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import QFileDialog, QInputDialog, QMessageBox, QSizePolicy
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import main


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1366, 720)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setObjectName("scroll_area")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.label.setAlignment(Qt.AlignTop)
        self.label.setGeometry(20, 20, 7680, 4800)
        self.scroll_area.setWidget(self.label)
        self.horizontalLayout_3.addWidget(self.scroll_area)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(5, 5, 1122, 40))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.preparation = QtWidgets.QMenu(self.menubar)
        self.preparation.setObjectName("preparation")
        self.gauss_filter = QtWidgets.QAction(self.menubar)
        self.gauss_filter.setObjectName("gauss_filter")
        self.canny_border = QtWidgets.QAction(self.menubar)
        self.canny_border.setObjectName("canny_border")

        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")

        MainWindow.setStatusBar(self.statusbar)
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionClose = QtWidgets.QAction(MainWindow)
        self.actionClose.setObjectName("actionClose")
        self.actionmoving = QtWidgets.QAction(MainWindow)
        self.actionmoving.setCheckable(False)
        self.action = QtWidgets.QAction(MainWindow)
        self.action.setObjectName("action")
        self.action_2 = QtWidgets.QAction(MainWindow)
        self.action_2.setObjectName("action_2")
        self.action_3 = QtWidgets.QAction(MainWindow)
        self.action_3.setObjectName("action_3")
        self.action_4 = QtWidgets.QAction(MainWindow)
        self.action_4.setObjectName("action_4")
        self.action_5 = QtWidgets.QAction(MainWindow)
        self.action_5.setObjectName("action_5")
        self.action_6 = QtWidgets.QAction(MainWindow)
        self.action_6.setObjectName("action_6")
        self.action_7 = QtWidgets.QAction(MainWindow)
        self.action_7.setObjectName("action_7")
        self.action_8 = QtWidgets.QAction(MainWindow)
        self.action_8.setObjectName("action_8")
        self.action_9 = QtWidgets.QAction(MainWindow)
        self.action_9.setObjectName("action_9")
        self.action_10 = QtWidgets.QAction(MainWindow)
        self.action_10.setObjectName("action_10")
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionClose)
        self.preparation.addAction(self.action)
        self.preparation.addAction(self.action_2)
        self.preparation.addAction(self.action_3)
        self.preparation.addAction(self.action_4)
        self.preparation.addAction(self.action_5)
        self.preparation.addAction(self.action_6)
        self.preparation.addAction(self.action_7)
        self.preparation.addAction(self.action_8)
        self.preparation.addAction(self.action_9)
        self.preparation.addAction(self.action_10)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.preparation.menuAction())
        self.menubar.addAction(self.gauss_filter)
        self.menubar.addAction(self.canny_border)

        self.retranslateUi(MainWindow)
        self.actionOpen.triggered.connect(self.load_image)
        self.actionSave.triggered.connect(self.save_photo)
        self.actionClose.triggered.connect(self.close)
        self.action.triggered.connect(self.one)
        self.action_2.triggered.connect(self.two)
        self.action_3.triggered.connect(self.three)
        self.action_4.triggered.connect(self.four)
        self.action_5.triggered.connect(self.five)
        self.action_6.triggered.connect(self.six)
        self.action_7.triggered.connect(self.seven)
        self.action_8.triggered.connect(self.eight)
        self.action_9.triggered.connect(self.nine)
        self.action_10.triggered.connect(self.ten)
        self.gauss_filter.triggered.connect(self.gauss)
        self.canny_border.triggered.connect(self.canny)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.filename = None
        self.tmp = None

    def load_image(self):
        """ This function will load the user selected image
            and set it to label using the setPhoto function
        """
        self.filename = QFileDialog.getOpenFileName(filter="Image (*.*)")[0]
        if len(self.filename) > 0:
            self.image = cv2.imread(self.filename)
            if type(self.image) is not np.ndarray:
                msg = QMessageBox()
                msg.setWindowTitle("Error dialog")
                msg.setText("Картинка не выбрана!")
                msg.setIcon(QMessageBox.Warning)
                self.filename = None
                x = msg.exec_()
                return
            self.set_photo(self.image)

    def re_load_image(self):
        if len(self.filename) > 0:
            image = cv2.imread(self.filename)
            if type(image) is not np.ndarray:
                msg = QMessageBox()
                msg.setWindowTitle("Error dialog")
                msg.setText("Картинка не выбрана!")
                msg.setIcon(QMessageBox.Warning)
                self.filename = None
                x = msg.exec_()
                return
            return image

    def set_photo(self, image):
        """ This function will take image input and resize it
            only for display purpose and convert it to QImage
            to set at the label.
        """

        self.tmp = image
        frame = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = QImage(frame, frame.shape[1], frame.shape[0], frame.strides[0], QImage.Format_RGB888)
        self.label.setPixmap(QtGui.QPixmap.fromImage(image))

    def save_photo(self):
        """ This function will save the image"""

        if self.exceptions_img(self.tmp):
            return
        self.filename = QFileDialog.getSaveFileName(filter="JPG(*.jpg);;PNG(*.png);;TIFF(*.tiff);;BMP(*.bmp)")[0]
        if len(self.filename) > 0:
            cv2.imwrite(self.filename, self.tmp)
            print('Image saved as:', self.filename)

    def one(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        if x < 0 or x > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.one(gray, x)
        self.set_photo(frame)


    def two(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x < 0 or x > 256 or y < 0 or y > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.two(gray, x, y)
        self.set_photo(frame)

    def three(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x < 0 or x > 256 or y < 0 or y > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.three(gray, x, y)
        self.set_photo(frame)

    def four(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        if x < 0 or x > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.four(gray, x)
        self.set_photo(frame)

    def five(self):
        if self.exceptions_img(self.tmp):
            return
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.five(gray)
        self.set_photo(frame)

    def six(self):
        if self.exceptions_img(self.tmp):
            return
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.six(gray)
        self.set_photo(frame)

    def seven(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x < 0 or x > 256 or y < 0 or y > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.seven(gray, x, y)
        self.set_photo(frame)

    def eight(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x < 0 or x > 256 or y < 0 or y > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.eight(gray, x, y)
        self.set_photo(frame)

    def nine(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x < 0 or x > 256 or y < 0 or y > 256:
            return self.cancel()
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.nine(gray, x, y)
        self.set_photo(frame)

    def ten(self):
        if self.exceptions_img(self.tmp):
            return
        n, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter n:')
        img = self.re_load_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        main.hist(gray)
        frame = main.ten(gray, n)

        self.set_photo(frame)

    def gauss(self):
        if self.exceptions_img(self.tmp):
            return
        x_size, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X_size:')
        y_size, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y_size:')
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x_size < 1 or y_size < 1 or y < 0 or x < 0:
            return self.cancel()
        img = self.re_load_image()
        frame = main.gauss(img, x_size, y_size, x, y)
        self.set_photo(frame)

    def canny(self):
        if self.exceptions_img(self.tmp):
            return
        x, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter X:')
        y, val_return1 = QInputDialog.getInt(self.centralwidget, 'Input dialog', 'Enter Y:')
        if x < 0 or x > 256 or y < 0 or y > 256:
            return self.cancel()
        img = self.re_load_image()
        frame = main.canny(img, x, y)
        self.set_photo(frame)

    def exceptions_img(self, image):
        if type(image) is not np.ndarray:
            msg = QMessageBox()
            msg.setWindowTitle("Error dialog")
            msg.setText("Картинка не выбрана!")
            msg.setIcon(QMessageBox.Warning)

            x = msg.exec_()
            return True

    def close(self):
        self.label.clear()
        self.tmp = None
        self.filename = None

    def cancel(self):
        msg = QMessageBox()
        msg.setWindowTitle("Cancel dialog")
        msg.setText("Функция отменена")
        msg.setIcon(QMessageBox.Information)

        x = msg.exec_()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Ваша картинка"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.preparation.setTitle(_translate("MainWindow", "Function preparation"))
        self.gauss_filter.setText(_translate("MainWindow", "Gauss filter"))
        self.canny_border.setText(_translate("MainWindow", "Canny Border Detector"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionSave.setText(_translate("MainWindow", "Save"))
        self.actionClose.setText(_translate("MainWindow", "Close"))
        self.action.setText(_translate("MainWindow", "а)"))
        self.action_2.setText(_translate("MainWindow", "б)"))
        self.action_3.setText(_translate("MainWindow", "в)"))
        self.action_4.setText(_translate("MainWindow", "г)"))
        self.action_5.setText(_translate("MainWindow", "д)"))
        self.action_6.setText(_translate("MainWindow", "е)"))
        self.action_7.setText(_translate("MainWindow", "ж)"))
        self.action_8.setText(_translate("MainWindow", "з)"))
        self.action_9.setText(_translate("MainWindow", "и)"))
        self.action_10.setText(_translate("MainWindow", "к)"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
