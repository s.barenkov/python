"""
убрать ylim в гистограмме или поставить больше огран
Изменить 9 метод
Подумать над 10 методом
изменить название гистограмм
"""
import math
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt


def hist(img):
    hist = cv.calcHist([img], [0], None, [256], [0, 256])
    plt.figure()
    plt.title(' Original hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()


def one(img, thr):
    ret, thresh = cv.threshold(img, thr, 255, cv.THRESH_BINARY)
    hist = cv.calcHist([thresh], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('A hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return thresh


def two(img, x, y):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if x < img[i, j] < y:
                img[i, j] = 255
            else:
                img[i, j] = 0
    hist = cv.calcHist([img], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('B hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return img


def three(img, x, y):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if x < img[i, j] < y:
                img[i, j] = 255
    hist = cv.calcHist([img], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('C hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return img


def four(img, x):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] > x:
                img[i, j] = 255
    hist = cv.calcHist([img], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('D hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return img


def five(img):
    image_cs = np.zeros((img.shape[0], img.shape[1]), dtype='uint8')
    xmin = np.amin(img)
    xmax = np.amax(img)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] > xmax:
                image_cs[i, j] = 255
            elif img[i, j] < xmin:
                image_cs[i, j] = 0
            else:
                image_cs[i, j] = 255 * (img[i, j] - xmin) / (xmax - xmin)
    hist = cv.calcHist([image_cs], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('E hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return image_cs


def six(img):
    xmin = np.amin(img)
    xmax = np.amax(img)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] >= xmax:
                img[i, j] = 0
            elif img[i, j] <= xmin:
                img[i, j] = 255
            else:
                img[i, j] = 255 - (255 * (img[i, j] - xmin) / (xmax - xmin))
    hist = cv.calcHist([img], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('F hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return img


def seven(img, x, y):
    image_cs = np.zeros((img.shape[0], img.shape[1]), dtype='uint8')
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] < x:
                image_cs[i, j] = 0
            elif img[i, j] > y:
                image_cs[i, j] = 0
            else:
                image_cs[i, j] = 255 * (img[i, j] - x) / (y - x)
    hist = cv.calcHist([image_cs], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('G hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return image_cs


def eight(img, x, y):
    image_cs = np.zeros((img.shape[0], img.shape[1]), dtype='uint8')
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] < x:
                image_cs[i, j] = 255
            elif img[i, j] > y:
                image_cs[i, j] = 255
            else:
                image_cs[i, j] = 255 * (img[i, j] - x) / (y - x)
    hist = cv.calcHist([image_cs], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('H hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return image_cs


"""добавить макс и мин, а также найти среднее значение пикселя"""
def nine(img, x, y):
    xmin = np.amin(img)
    xmax = np.amax(img)
    image_cs = np.zeros((img.shape[0], img.shape[1]), dtype='uint8')
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] < x:
                image_cs[i, j] = 128
            elif img[i, j] > y:
                image_cs[i, j] = 128
            else:
                image_cs[i, j] = 255 * (img[i, j] - x) / (y - x)
    hist = cv.calcHist([image_cs], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('I hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return image_cs


def ten(img, n):
    dim = int(math.floor(256 / n))
    x = 0
    y = dim - 1
    for i in range(len(img)):
        for j in range(len(img[i])):
            pldim = 0
            while img[i, j] > dim:
                img[i, j] = img[i, j] - dim
                pldim = pldim + 1
            img[i, j] = img[i, j] + 1
            img[i, j] = (255 * (img[i, j] - x) / (y - x))
            while pldim > 0:
                img[i, j] = img[i, j] + dim
                pldim = pldim - 1
    hist = cv.calcHist([img], [0], None, [256], [0, 256])
    plt.figure()
    plt.title('I hist')
    plt.xlabel('Bins')
    plt.ylabel('num of pix')
    plt.plot(hist)
    plt.xlim([0, 256])
    plt.ylim([0, 10000])
    plt.show()
    return img


def gauss(img, x_size, y_size, x, y):
    blur = cv.GaussianBlur(img, (x_size, y_size), x, sigmaY=y)
    return blur


def canny(img, x, y):
    canny = cv.Canny(img, x, y)
    return canny


# one(gray, 125)

# two(img, 50, 150)
# three(img, 50, 150)
# four(img, 125)
# five(img)
# six(img)
# seven(img, 50, 150)
# eight(img, 50, 150)
# nine(img, 50, 150)
# ten(img, 5)
# gauss(img, 5, 5, 2, 2)
# canny(img, 100, 100)
# cv.waitKey(0)
cv.destroyAllWindows()
