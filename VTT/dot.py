import glob

import cv2 as cv
import numpy as np

ready_to_video = []


def create_video(imgs):
    height, width = imgs[0].shape[:2]
    fourcc = cv.VideoWriter_fourcc(*'mp4v')
    video = cv.VideoWriter(r'video.mp4', fourcc, 30, (width, height))
    for fg_frame in imgs:
        video.write(fg_frame)
    video.release()
    cap = cv.VideoCapture(r'E:\IDE python projects\VTT\video.mp4')
    if not cap.isOpened():
        print("Error opening video  file")
    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            cv.imshow('Frame', frame)
            if cv.waitKey(25) & 0xFF == ord('q'):
                break
            else:
                break
    cap.release()


def rescale_frame(frame):
    width = 1366
    height = 720
    dimensions = (width, height)
    output = cv.resize(frame, dimensions, interpolation=cv.INTER_CUBIC)
    return output


def create_train_img():
    images = [cv.imread(file) for file in glob.glob(r"E:\IDE java projects\frames\Andy\frames\*.jpg")]
    return images


def create_train_mask():
    masks = [cv.imread(file) for file in glob.glob(r"E:\IDE java projects\frames\Andy\masks\*.jpg")]
    return masks


def create_dot_mask():
    mask = create_train_mask()
    img = create_train_img()
    for i in range(len(mask)):
        blank = np.zeros((720, 1366, 3), dtype='uint8')
        gray = cv.cvtColor(rescale_frame(mask[i]), cv.COLOR_BGR2GRAY)
        ret, thresh = cv.threshold(rescale_frame(gray), 150, 255, cv.THRESH_BINARY)

        contours, _ = cv.findContours(rescale_frame(thresh), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

        point = contours[0].reshape(-1, 2)
        for k in range(len(point)):
            if k % 10 == 0:
                t = tuple(point[k])
                # cv.drawContours(blank, contours, -1, (255, 0, 0), 2)
                cv.circle(blank, t, 4, (100, 0, 0), thickness=-1)
                cv.circle(blank, t, 2, (255, 0, 0), thickness=-1)

        bin_and = cv.bitwise_or(rescale_frame(img[i]), blank)
        ready_to_video.append(bin_and)
    create_video(ready_to_video)


create_dot_mask()

cv.waitKey(0)
